#!/bin/bash
echo $(date)
fakelastsun=$(date -d "2024-02-25")
echo "fake last sun: " $fakelastsun
fakelastsat=$(date -d "2024-02-24")
echo "fake last sat: " $fakelastsat
fakelastmon=$(date -d "2024-02-26")
echo "fake last mon: " $fakelastmon
startdate=$(date -d "2024-02-01")
daysinmonth=29
unit="days"
echo "startdate: " $startdate " days in month: " $daysinmonth
for ((i=0 ; i<($daysinmonth) ; i++)); do
   incr=$i$unit
   testday=$(date -d "$startdate + $incr")
   echo "testing. i: " $i " incr: "$incr " testday: " $testday
   # test if sunday
   if [[ $(date -d "$testday" +%w) == 0  ]];
   then
      echo "is a sunday"
      if [[ $(date -d "$testday +1week" +%m) != $(date -d "$testday" +%m)  ]];
      then
         echo "IS THE LAST SUNDAY"
      fi
   elif [[ $(date -d "$testday" +%w) == 6  ]];
   then
      echo "is a saturday"
   else
      echo "regular DOW"
   fi
done
